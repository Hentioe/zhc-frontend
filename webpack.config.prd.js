const path = require('path');
const webpack = require('webpack');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const qn = require('./deploy/qn');
const up = require('./deploy/upRes');
const buildPath = path.resolve(__dirname, 'dist');
const srcPath = __dirname + '/src';

module.exports = {
    entry: {
        front: ['babel-polyfill', srcPath + '/forestage/app.jsx'],
    },
    output: {
        path: buildPath,
        filename: '[name].[hash].js',
        publicPath: '/dist/'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules(?!\/striptags)|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'stage-2']
                    }
                }
            },
            {
                test: /\.jsx$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['react', 'env', 'stage-2']
                    }
                }
            },
            {
                test: /\.(scss|sass)$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"},
                    {loader: "sass-loader"}
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"}
                ]
            },
            {
                test: /\.(png|jpg|ttf|eot|svg|woff|woff2)$/,
                loader: 'url-loader'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                drop_console: true,
                warnings: true
            }
        }),
        new CleanWebpackPlugin(['dist'], {
            root: path.resolve(__dirname),
            verbose: true,
            dry: false,
            exclude: ['.gitignore']
        }),
        function () {
            this.plugin("done", stats => {
                const suffix = '.' + stats.hash + '.js';
                // 上传资源至 CDN
                qn.upload('./dist/front' + suffix);
                // 更新后端资源 hash 缓存
                up.push(stats.hash);
            });
        }
    ]
};
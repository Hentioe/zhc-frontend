(function (doc, win) {
        let metaEl = doc.querySelector('meta[name="viewport"]');
        let dpr = 0;
        let scale = 0;

        if (!dpr && !scale) {
            dpr = 1;
            let isIPhone = (/(iphone|ipad)/gi).test(win.navigator.appVersion);
            // if (isIPhone) dpr = win.devicePixelRatio;
            scale = 1 / dpr;
        }

        // 动态改写 meta:viewport 标签
        if (!metaEl) {
            metaEl = doc.createElement('meta');
            metaEl.setAttribute('name', 'viewport');
            metaEl.setAttribute('content', 'width=device-width, initial-scale=' + scale + ', maximum-scale=' + scale + ', minimum-scale=' + scale + ', user-scalable=no');
            document.documentElement.firstElementChild.appendChild(metaEl);
        } else {
            metaEl.setAttribute('content', 'width=device-width, initial-scale=' + scale + ', maximum-scale=' + scale + ', minimum-scale=' + scale + ', user-scalable=no');
        }
    }
)(document, window);
import {
    REQUEST_FEED, RECEIVE_FEED
} from './actions'

import {combineReducers} from 'redux'

const defaultFeedState = {
    isLoading: true,
    data: {
        data: []
    }
};

function feed(state = defaultFeedState, action) {
    switch (action.type) {
        case REQUEST_FEED:
            return Object.assign({}, state, {isLoading: true});
        case RECEIVE_FEED:
            return Object.assign({}, state, {isLoading: false, data: action.data});
    }
    return state;
}

export default combineReducers({
    feed
});
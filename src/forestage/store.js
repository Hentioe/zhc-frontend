import thunkMiddleware from "redux-thunk";
import {applyMiddleware, createStore} from "redux";
import {logger} from "redux-logger";
import reducers from "./reducers";


const DEBUG = process.env['NODE_ENV'] !== 'production';

const middleWares = [
    thunkMiddleware,
    DEBUG && logger
].filter(Boolean);
export default createStore(reducers, applyMiddleware(...middleWares));
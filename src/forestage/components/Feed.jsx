import React from 'react'
import striptags from 'striptags'
import moment from 'moment'

moment.locale('zh-cn');

class Content extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            full: false
        };

        this.readMore = this.readMore.bind(this);
    }

    readMore() {
        this.setState({
            full: true
        });
    }

    render() {

        const content = striptags(this.props.content, '<p>');
        const excerpt = striptags(this.props.excerpt);

        return (<div className="feed-item-content">
            {
                this.state.full ? <div dangerouslySetInnerHTML={{__html: content}}/> : <div><span>{excerpt}</span>
                    {
                        content && excerpt !== content
                        && <span onClick={this.readMore} className="feed-item-content-more">阅读全文</span>
                    }
                </div>
            }

        </div>)
    }
}

Array.prototype.propCombine = function (key, c) {
    let tmlText = '';
    this.forEach((o, i) => {
        tmlText += o[key];
        if ((i + 1) !== this.length) {
            tmlText += c;
        } else {
            if (/[a-zA-Z]/.test(o[key].slice(-1)[0]))
                tmlText += ' ';
        }
    });
    return tmlText;
};

String.prototype.suffix = function (reg, suffixContent, index) {
    let tmpContent = this;
    if (index)
        tmpContent = this.slice(index)[0];
    if (reg.test(tmpContent))
        return this + suffixContent;
    return this;
};


Number.prototype.utsFromNow = function () {
    return moment(this.toString(), 'X').fromNow();
};

class Feed extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const {verb, target, actors, created_time} = this.props.data;
        let tmpActorsVerb, tmpTitle, tmpExcerpt, tmpContent, tmpUrl = undefined;
        switch (verb) {
            // 来自话题的热门回答（非关注着）
            case 'TOPIC_ACKNOWLEDGED_ANSWER':
                tmpActorsVerb = `${target.author.name.suffix(/[a-zA-Z]/, ' ', -1)}回答了问题 · 来自话题: ${actors.propCombine('name', '、')}`;
                tmpTitle = target.question.title;
                tmpExcerpt = target.excerpt;
                tmpContent = target.content;
                tmpUrl = `https://www.zhihu.com/question/${target.question.id}/answer/${target.id}`;
                break;
            // 关注人回答了问题
            case 'MEMBER_ANSWER_QUESTION':
                tmpActorsVerb = `${target.author.name.suffix(/[a-zA-Z]/, ' ', -1)}回答了问题 · ${created_time.utsFromNow()}`;
                tmpTitle = target.question.title;
                tmpExcerpt = target.excerpt;
                tmpContent = target.content;
                tmpUrl = `https://www.zhihu.com/question/${target.question.id}/answer/${target.id}`;
                break;
            // 赞同了回答
            case 'MEMBER_VOTEUP_ANSWER':
                tmpActorsVerb = `${actors.propCombine('name', '、')}赞同了回答 · ${created_time.utsFromNow()}`;
                tmpTitle = target.question.title;
                tmpExcerpt = target.excerpt;
                tmpContent = target.content;
                tmpUrl = `https://www.zhihu.com/question/${target.question.id}/answer/${target.id}`;
                break;
            // 收藏了回答
            case 'MEMBER_COLLECT_ANSWER':
                tmpActorsVerb = `${actors.propCombine('name', '、')}收藏了回答 · ${created_time.utsFromNow()}`;
                tmpTitle = target.question.title;
                tmpExcerpt = target.excerpt;
                tmpContent = target.content;
                tmpUrl = `https://www.zhihu.com/question/${target.question.id}/answer/${target.id}`;
                break;
            // 关注了问题
            case 'MEMBER_FOLLOW_QUESTION':
                tmpActorsVerb = `${actors.propCombine('name', '、')}关注了问题 · ${created_time.utsFromNow()}`;
                tmpTitle = target.title;
                tmpExcerpt = target.excerpt;
                tmpContent = target.content;
                tmpUrl = `https://www.zhihu.com/question/${target.id}`;
                break;
            // 发表了文章
            case "MEMBER_CREATE_ARTICLE":
                tmpActorsVerb = `${actors.propCombine('name', '、')}发表了文章 · ${created_time.utsFromNow()}`;
                tmpTitle = target.title;
                tmpExcerpt = target.excerpt;
                tmpContent = target.content;
                tmpUrl = `https://zhuanlan.zhihu.com/p/${target.id}`;
                break;
            // 关注专栏新增文章
            case "COLUMN_NEW_ARTICLE":
                tmpActorsVerb = `${actors.propCombine('name', '、')}发表了文章 · ${created_time.utsFromNow()}`;
                tmpTitle = target.title;
                tmpExcerpt = target.excerpt;
                tmpContent = target.content;
                tmpUrl = `https://zhuanlan.zhihu.com/p/${target.id}`;
                break;
            // 来自话题热门文章（未关注）
            case "COLUMN_POPULAR_ARTICLE":
                tmpActorsVerb = `${target.author.name.suffix(/[a-zA-Z]/, ' ', -1)}发表了文章 · 来自专栏: ${actors.propCombine('title', '、')}`;
                tmpTitle = target.title;
                tmpExcerpt = target.excerpt;
                tmpContent = target.content;
                tmpUrl = `https://zhuanlan.zhihu.com/p/${target.id}`;
                break;
            // 点赞了文章
            case "MEMBER_VOTEUP_ARTICLE":
                tmpActorsVerb = `${actors.propCombine('name', '、')}赞了文章 · ${created_time.utsFromNow()}`;
                tmpTitle = target.title;
                tmpExcerpt = target.excerpt;
                tmpContent = target.content;
                tmpUrl = `https://zhuanlan.zhihu.com/p/${target.id}`;
                break;
            // 关注了专栏
            case "MEMBER_FOLLOW_COLUMN":
                tmpActorsVerb = `${actors.propCombine('name', '、')}关注了专栏 · ${created_time.utsFromNow()}`;
                tmpTitle = target.title;
                tmpExcerpt = `简介：${target.description}`;
                tmpUrl = `https://zhuanlan.zhihu.com/${target.id}`;
                break;
            // 关注了圆桌
            case "MEMBER_FOLLOW_ROUNDTABLE":
                tmpActorsVerb = `${actors.propCombine('name', '、')}关注了圆桌 · ${created_time.utsFromNow()}`;
                tmpTitle = target.name;
                tmpExcerpt = target.description;
                tmpUrl = `https://www.zhihu.com/roundtable/${target.id}`;
                break;
            // 来自话题下的问题
            case "TOPIC_POPULAR_QUESTION":
                tmpActorsVerb = `热门问题推送 ·来自话题: ${actors.propCombine('name', '、')}`;
                tmpTitle = target.title;
                tmpExcerpt = target.excerpt;
                tmpContent = target.detail;
                tmpUrl = `https://www.zhihu.com/question/${target.id}`;
                break;

            // 添加了问题
            case "MEMBER_ASK_QUESTION":
                tmpActorsVerb = `${actors.propCombine('name', '、')}添加了问题 · ${created_time.utsFromNow()}`;
                tmpTitle = target.title;
                tmpExcerpt = target.excerpt;
                tmpContent = target.detail;
                tmpUrl = `https://www.zhihu.com/question/${target.id}`;
                break;
        }
        return (
            <div className="feed-item">
                <div>
                    <div className="feed-item-from">
                        <span>
                            {tmpActorsVerb}
                        </span>
                    </div>
                    <div className="feed-item-title">
                        <a target="_blank" href={tmpUrl}>
                            {tmpTitle}
                        </a>
                    </div>
                    <Content excerpt={tmpExcerpt} content={tmpContent}/>
                </div>
            </div>
        )
    }
}

export default Feed;
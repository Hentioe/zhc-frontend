import React from 'react'
import {Route} from "react-router-dom"
import ListBody from './ListBody'

export default (props) => {
    return (
        <div id="container">
            <Route exact path="/" component={ListBody}/>
        </div>
    )
}
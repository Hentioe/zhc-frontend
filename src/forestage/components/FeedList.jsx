import React from 'react'
import {connect} from 'react-redux'
import DocumentTitle from 'react-document-title'

import Feed from './Feed'

import {fetchFeed} from '../actions'


class FeedList extends React.Component {

    constructor(props) {
        super(props);

        this.page = 1;
        this.requestLock = false;
        this.allData = [];

        this.handleScroll = this.handleScroll.bind(this);
    }

    componentWillUnmount() {
        document.removeEventListener('scroll', this.handleScroll);
    }

    componentDidMount() {
        document.addEventListener('scroll', this.handleScroll);
        const {dispatch} = this.props;
        dispatch(fetchFeed(this.page++));
    }

    componentDidUpdate(){
        // 防止内容没有窗口高度长而导致无法触发下一页加载
        if (this.allData.length < 7) this.handleScroll();
    }


    handleScroll() {
        if ((window.innerHeight + window.scrollY + 200) >= document.body.offsetHeight) {
            // 请求下一页
            if (!this.requestLock) {
                const {dispatch} = this.props;
                dispatch(fetchFeed(this.page++));
                this.requestLock = true;
            }
        }
    }

    render() {
        const {data, isLoading} = this.props;
        if (!isLoading) {
            !isLoading && this.allData.push(...data.data);
            if (this.page > 1) this.requestLock = false;
        }
        return (
            <div id="feed-list">
                <DocumentTitle title="知乎纯净版 - 首页 | from 绅士喵"/>
                {
                    this.allData.map(((feed, i) => {
                        return <Feed key={i} data={feed}/>
                    }))
                }
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        isLoading: state.feed.isLoading,
        data: state.feed.data
    }
}

export default connect(mapStateToProps)(FeedList);

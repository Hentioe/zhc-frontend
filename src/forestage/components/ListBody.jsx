import React from 'react'
import {Route} from "react-router-dom"

import FeedList from '../components/FeedList'

export default () => {
    return (
        <div id="home-list">
            <Route exact path="/" component={FeedList}/>
        </div>
    )
}
import React from 'react'
import {BrowserRouter as Router} from "react-router-dom"

import TopBar from '../components/TopBar'
import Container from '../components/Container'

import ispe from "../../lib/ispe";
import '../style/index.scss';

const Index = () => (
    <Router>
        <div id="app" ispe={ispe}>
            <TopBar/>
            <Container/>
        </div>
    </Router>
);

export default Index;
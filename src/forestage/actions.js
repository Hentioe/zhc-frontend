import fetch from 'cross-fetch'

export const REQUEST_FEED = "REQUEST_FEED";
export const RECEIVE_FEED = "RECEIVE_FEED";


export function requestFeed() {
    return {
        type: REQUEST_FEED
    }
}


export function receiveFeed(data) {
    return {
        type: RECEIVE_FEED,
        data: data
    }
}

export function fetchFeed(page) {
    return dispatch => {
        dispatch(requestFeed());
        fetch(`https://zhclean-api.bluerain.io/feed/${page}`)
            .then(resp => resp.json())
            .then(json => dispatch(receiveFeed(json)))
    }
}
import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import store from './store'

import '../lib/autoviewport'

import Index from './views/Index'

render(
    <Provider store={store}>
        <Index/>
    </Provider>,
    document.getElementById('root')
);
const path = require('path');
const buildPath = path.resolve(__dirname, 'dist');
const srcPath = __dirname + '/src';

module.exports = {
    entry: {
        front: ['babel-polyfill', srcPath + '/forestage/app.jsx']
    },
    output: {
        path: buildPath,
        filename: '[name].js',
        publicPath: '/dist/'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules(?!\/striptags)|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env', 'stage-2']
                    }
                }
            },
            {
                test: /\.jsx$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['react', 'env', 'stage-2']
                    }
                }
            },
            {
                test: /\.(scss|sass)$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"},
                    {loader: "sass-loader"}
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {loader: "style-loader"},
                    {loader: "css-loader"}
                ]
            },
            {
                test: /\.(png|jpg|ttf|eot|svg|woff|woff2)$/,
                loader: 'url-loader'
            }
        ]
    },
    devServer: {
        port: 8080,
        inline: true,
        host: '0.0.0.0',
        historyApiFallback: {
            rewrites: [
                {from: /^\/?$/, to: '/index.html'},
                {from: /./, to: '/404.html'},
            ],
        },
    }
};
/**
 * Created by hentioe on 16-11-14.
 */
const Config = require('./config').Config;
const qiniu = require("qiniu");
const mac = new qiniu.auth.digest.Mac(Config.ACCESS_KEY, Config.SECRET_KEY);
const options = {
    scope: Config.BUCKET,
};
const putPolicy = new qiniu.rs.PutPolicy(options);
const uploadToken = putPolicy.uploadToken(mac);

//生成上传 Token
qiniu.conf.ACCESS_KEY = Config.ACCESS_KEY;
qiniu.conf.SECRET_KEY = Config.SECRET_KEY;

//构造上传函数
function uploadFile(file_key, file_path) {
    let config = new qiniu.conf.Config();
    config.zone = qiniu.zone.Zone_z0;
    const formUploader = new qiniu.form_up.FormUploader(config);
    const putExtra = new qiniu.form_up.PutExtra();
    // 文件上传
    formUploader.putFile(uploadToken, file_key, file_path, putExtra, function (err, ret) {
        if (!err) {
            // 上传成功， 处理返回值
            console.log('File: ' + file_key + ' upload success!');
        } else {
            // 上传失败， 处理返回代码
            console.log(err);
        }
    });
}

exports.upload = function (file_path) {
    uploadFile('zhclean/' + file_path.match(/[^\/]+$/)[0], file_path);
};
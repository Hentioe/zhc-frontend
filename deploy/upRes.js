const fetch = require('cross-fetch');

function handleErr(e) {
    console.error('Deploy error: ' + e.message);
}

exports.push = function (hash) {
    fetch('http://rvc/core/zhclean/version', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "val": hash
        })
    })
        .then(resp => resp.json())
        .then(json => {
            if (json.code === 0)
                console.log('Deploy success!');
            else
                throw new Error('Update resources api invoke failed!');
        })
        .catch(handleErr)
};